import os
import sys

from django import setup


def pytest_configure():
    """setup django"""
    sys.path.append(os.path.abspath('./tests/testproject'))
    os.environ['DJANGO_SETTINGS_MODULE'] = 'testproject.settings'
    setup()
