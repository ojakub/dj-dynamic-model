"""Tests for dynamic model"""
from django.contrib.auth.models import User

import pytest
from model_mommy import mommy

import dynamic_model


@pytest.mark.parametrize(
    ('inp', 'exp'), (
        ('lol_wut', 'LolWut'),
        ('tab0$le', 'Tab0Le'),
        ('tablename', 'Tablename')))
def test_table2model(inp, exp):
    """Few cases"""
    actual = dynamic_model._table2model(inp)
    assert actual == exp


@pytest.mark.django_db
def test_model_factory():
    """Tests model factory"""
    mommy.make('auth.User', _quantity=10)
    dyn_user = dynamic_model.model_factory('auth_user')

    expected = User.objects.values_list('username', flat=True)
    actual = dyn_user.objects.values_list('username', flat=True)

    assert sorted(expected) == sorted(actual)


@pytest.mark.django_db
def test_raises_on_invalid_table():
    """If invalid table is used ``ModelError`` should be raised"""
    with pytest.raises(dynamic_model.ModelError):
        dynamic_model.model_factory('not a table')
