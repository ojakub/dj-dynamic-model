from setuptools import setup

import dynamic_model

setup(
    name='django-dynamic-model',
    version=dynamic_model.__version__,
    url='https://gitlab.com/jakubo/dj-dynamic-model',
    description=dynamic_model.__doc__,
    packages=['dynamic_model'],
    license='MIT',
    long_description=open('README').read(),
    install_requires=['django>=1.10.5'],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3',
    ]
)
